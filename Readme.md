# Тестовое в Эмбедику.

CRUD API с машинами.

## Общая информация

Для работы использует БД Postgres, учетные данные от базы нужно передать через переменные среды `DB_URL`, `DB_USR`, `DB_PASS`.
Либо можно передать параметр запуска -Dconfig-path=, где должен быть конфиг аналогичный лежащему в resources/application.conf.

Если не менять application.conf, то запускается на порту 3000.

Для сборки Docker образа используется sbt-native-packager, sbt docker:publishLocal соберет образ локально.

Swagger API доступен на данном URL `/swagger/index.html`

На `/metrics` доступны Prometheus совместимые метрики HTTP запросов.

## Архитектура

Приложение написано с использованием ZIO. 
Сборка зависимостей происходит в Application.scala с использованием ZIO magic. 
Для парсинга конфигурационных файлов используется pureconf

HTTP эндпоинты описаны с использованием Tapir и находятся в infra/modules. 

В качестве HTTP сервера используется ZIO-http, запуск происходит в infra/http.server/HttpServer.scala, эндпоинты преобразуются в совместимый с ZIO-http формат в infra/modules/Modules.scala

Логика эндпоинтов реализованна в сервисах, которые находятся в services.

Сервисы для хранения информации обращаются к репозиториям находящимся в repositories.

Для работы с базой данных используется Doobie, его транзактор создается в infra/db/DbSessionProvider.scala. 

Для логирования используется ZIO-logging с SLF4J. Запросы логируются через infra/logging/HttpRequestLogger.scala, благодаря Tapir Intersector.

Матрики тоже собирает Tapir.