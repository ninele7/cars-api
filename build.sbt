import Dependencies._
import sbt._

ThisBuild / scalaVersion := "2.13.8"
ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / organization := "com.example"
ThisBuild / organizationName := "example"

enablePlugins(DockerPlugin)
enablePlugins(JavaAppPackaging)

val dockerSettings = Seq(
  dockerBaseImage := "openjdk:11-jre",
  dockerExposedPorts := Seq(3000)
)

lazy val root = (project in file("."))
  .settings(
    name := "cars-api",
    libraryDependencies ++= Seq(
      zio.core,
      zio.magic,
      zio.http,
      zio.catz,
      zio.logging,
      zio.loggingSlf4j,
      tapir.core,
      tapir.zio,
      tapir.swagger,
      tapir.jsonCirce,
      tapir.refined,
      tapir.prometheus,
      refined.core,
      pureConfig.base,
      tofu.derevo,
      tofu.derevoCirce,
      circe.circeExtras,
      circe.refined,
      infra.logback,
      doobie.base,
      doobie.hikary,
      doobie.postgres,
      doobie.refined
//      caliban.core,
//      caliban.tapir,
//      caliban.zHttp
    ),
    testFrameworks += new TestFramework("zio.test.sbt.ZTestFramework"),
    scalacOptions += "-Ymacro-annotations"
  )
  .settings(dockerSettings)

addCompilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.1")
addCompilerPlugin(
  "org.typelevel" % "kind-projector" % "0.13.2" cross CrossVersion.full
)
