import sbt._

object Dependencies {
  object zio {
    lazy val core = "dev.zio" %% "zio" % "1.0.12"
    lazy val magic = "io.github.kitlangton" %% "zio-magic" % "0.3.11"
    lazy val http = "io.d11" %% "zhttp" % "1.0.0.0-RC23"
    lazy val catz = "dev.zio" %% "zio-interop-cats" % "3.2.9.0"
    lazy val logging = "dev.zio" %% "zio-logging" % "0.5.14"
    lazy val loggingSlf4j = "dev.zio" %% "zio-logging-slf4j" % "0.5.14"
  }

  object tapir {
    lazy val tapirVersion = "0.20.0-M7"
    lazy val core = "com.softwaremill.sttp.tapir" %% "tapir-core" % tapirVersion
    lazy val zio =
      "com.softwaremill.sttp.tapir" %% "tapir-zio-http-server" % tapirVersion
    lazy val swagger =
      "com.softwaremill.sttp.tapir" %% "tapir-swagger-ui-bundle" % tapirVersion
    lazy val jsonCirce =
      "com.softwaremill.sttp.tapir" %% "tapir-json-circe" % tapirVersion
    lazy val refined =
      "com.softwaremill.sttp.tapir" %% "tapir-refined" % tapirVersion
    lazy val prometheus =
      "com.softwaremill.sttp.tapir" %% "tapir-prometheus-metrics" % tapirVersion
  }

  object refined {
    lazy val core = "eu.timepit" %% "refined" % "0.9.28"
  }

  object circe {
    lazy val circeVersion = "0.14.1"
    lazy val circeExtras = "io.circe" %% "circe-generic-extras" % circeVersion
    lazy val refined = "io.circe" %% "circe-refined" % circeVersion
  }

  object pureConfig {
    lazy val base = "com.github.pureconfig" %% "pureconfig" % "0.17.1"
  }

  object tofu {
    lazy val derevoVersion = "0.12.7"
    lazy val derevo = "tf.tofu" %% "derevo-core" % derevoVersion
    lazy val derevoCirce = "tf.tofu" %% "derevo-circe" % derevoVersion
  }

  object infra {
    lazy val logback = "ch.qos.logback" % "logback-classic" % "1.2.10"
  }

  object doobie {
    lazy val doobieVersion = "1.0.0-RC2"
    lazy val base = "org.tpolecat" %% "doobie-core" % doobieVersion
    lazy val hikary = "org.tpolecat" %% "doobie-hikari" % doobieVersion
    lazy val postgres = "org.tpolecat" %% "doobie-postgres" % doobieVersion
    lazy val refined = "org.tpolecat" %% "doobie-refined" % doobieVersion
  }

  object enumeratum {
    lazy val enumeratumVersion = "1.7.0"
    lazy val core = "com.beachape" %% "enumeratum" % enumeratumVersion
  }

  object caliban {
    lazy val calibanVersion = "1.3.3"
    lazy val core = "com.github.ghostdogpr" %% "caliban" % calibanVersion
    lazy val tapir = "com.github.ghostdogpr" %% "caliban-tapir" % calibanVersion
    lazy val zHttp =
      "com.github.ghostdogpr" %% "caliban-zio-http" % calibanVersion
  }
}
