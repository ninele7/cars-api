import sbt.Keys.resolvers
import sbt._

resolvers += Resolver.sbtPluginRepo("releases")

addSbtPlugin("com.github.sbt" % "sbt-native-packager" % "1.9.7")
