create table cars
(
    registration_plate char(8) primary key check (
            registration_plate similar to
            '[A-Z][A-Z][0-9][0-9][0-9][A-Z][0-9][0-9]'
        ),
    manufacturer       varchar,
    color              varchar,
    production_year    int check (production_year > 0 ),
    created_at         timestamp default current_timestamp
);