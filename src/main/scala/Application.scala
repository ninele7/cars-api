import config.Config
import infra.db.DbSessionProvider
import infra.http.server.HttpServer
import infra.logging.{AppLogger, HttpRequestLogger}
import infra.modules.{CarsModule, HelloWorldModule, Modules}
import repositories.CarsRepository
import services.CarsService
import zio._
import zio.magic._

class Application {
  def start: URIO[ZEnv, ExitCode] =
    HttpServer.start
      .injectCustom(
        HelloWorldModule.layer,
        DbSessionProvider.layer,
        CarsRepository.layer,
        CarsService.layer,
        CarsModule.layer,
        HttpRequestLogger.layer,
        Modules.layer,
        HttpServer.layer,
        Config.layer,
        AppLogger.layer
      )
      .exitCode
}
