package config

import zio.{Has, ZIO}

import pureconfig._
import pureconfig.error.ConfigReaderFailures
import pureconfig.generic.auto._

import scala.jdk.CollectionConverters._

case class HttpServerConfig(port: Int)
case class DbConfig(url: String, user: String, password: String)

case class Config(httpServer: HttpServerConfig, db: DbConfig)

object Config {
  case class ConfigReadError(failures: ConfigReaderFailures) extends Exception {
    override def getMessage: String = failures.toString()
  }

  val layer =
    ZIO.fromEither {
      System.getProperties.asScala
        .get("config-path")
        .fold(ConfigSource.default)(ConfigSource.file)
        .load[Config]
        .map(cfg => Has.allOf(cfg.httpServer, cfg.db))
        .left
        .map(ConfigReadError)
    }.toLayerMany

}
