package domain

import eu.timepit.refined.api.Refined
import eu.timepit.refined.numeric.Greater
import eu.timepit.refined.string.MatchesRegex
import eu.timepit.refined.auto._

import java.time.OffsetDateTime

object Cars {
  type CarRegistrationPlate = String Refined MatchesRegex[
    "[A-Z]{2}\\d{3}[A-Z]\\d{2}"
  ]

  type Year = Int Refined Greater[0]

  case class Car(
      registrationPlate: CarRegistrationPlate,
      manufacturer: String,
      color: String,
      productionYear: Year
  )

  case class CarsStats(
      amount: Int,
      firstAdded: Option[OffsetDateTime],
      lastAdded: Option[OffsetDateTime]
  )
}
