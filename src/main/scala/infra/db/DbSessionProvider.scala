package infra.db

import config.DbConfig
import doobie.util.transactor.Transactor
import zio._
import zio.interop.catz._
import zio.interop.catz.implicits._

object DbSessionProvider {
  val layer: RLayer[Has[DbConfig], Has[Transactor[Task]]] = ZLayer.fromService { (cfg: DbConfig) =>
    Transactor
      .fromDriverManager[Task](driver = "org.postgresql.Driver", url = cfg.url, user = cfg.user, pass = cfg.password)
  }
}
