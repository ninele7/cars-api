package infra

import enumeratum.{Enum, EnumEntry}

package object db {
  sealed trait OrderDirection extends EnumEntry

  object OrderDirection extends Enum[OrderDirection] {
    val values: IndexedSeq[OrderDirection] = findValues

    case object Ascending extends OrderDirection
    case object Descending extends OrderDirection
  }

  sealed trait CarsError extends RuntimeException

  object CarAlreadyExists extends CarsError {
    override def getMessage: String = "Car Already exists"
  }
  object CarNotPresent extends CarsError {
    override def getMessage: String = "Car not present"
  }

  case object IncorrectOrderingField extends CarsError {
    override def getMessage: String = "Incorrect ordering field"
  }
}
