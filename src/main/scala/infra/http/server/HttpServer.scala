package infra.http.server

import config.HttpServerConfig
import infra.modules.Modules
import zio._
import zio.logging.Logger
import zhttp.service.Server

case class HttpServer(
    modules: Modules,
    config: HttpServerConfig,
    logger: Logger[String]
) {
  def start: RIO[ZEnv, Unit] =
    for {
      _ <- logger.info(s"Starting server on port ${config.port}")
      endPoint = modules.httpEndpoint
      _ <- Server.start(config.port, endPoint)
    } yield ()

}

object HttpServer {
  def start: RIO[Has[HttpServer] with ZEnv, Unit] =
    ZIO.service[HttpServer] >>= (_.start)

  val layer = (HttpServer.apply _).toLayer
}
