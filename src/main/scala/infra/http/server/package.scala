package infra.http

import zhttp.http.HttpApp

package object server {
  type AppEndpoint = HttpApp[Any, Throwable]
}
