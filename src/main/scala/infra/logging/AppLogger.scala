package infra.logging

import zio.logging.LogAnnotation
import zio.logging.slf4j.Slf4jLogger

object AppLogger {
  val layer = Slf4jLogger.make { (context, message) =>
    val correlationId = context.get(LogAnnotation.CorrelationId)
    correlationId match {
      case Some(uuid) => s"[trackingId: $uuid] $message"
      case None       => message
    }
  }
}
