package infra.logging

import sttp.tapir.AnyEndpoint
import sttp.tapir.model.{ServerRequest, ServerResponse}
import sttp.tapir.server.interceptor.log.ServerLog
import sttp.tapir.server.interceptor.{DecodeFailureContext, DecodeSuccessContext, SecurityFailureContext}
import zio.{Has, Task, URLayer}
import zio.logging.Logger

case class HttpRequestLogger(logger: Logger[String]) extends ServerLog[Task] {
  override def decodeFailureNotHandled(
      ctx: DecodeFailureContext
  ): Task[Unit] =
    Task.unit

  override def decodeFailureHandled(
      ctx: DecodeFailureContext,
      response: ServerResponse[_]
  ): Task[Unit] = logger.log(
    s"Request handled by: ${ctx.endpoint.show}; decode failure: ${ctx.failure}, on input: ${ctx.failingInput.show}; responding with: $response"
  )

  override def securityFailureHandled(
      ctx: SecurityFailureContext[Task, _],
      response: ServerResponse[_]
  ): Task[Unit] =
    logger.log(
      s"Request ${ctx.request} handled by: ${ctx.endpoint.show}; security logic error response: $response"
    )

  override def requestHandled(
      ctx: DecodeSuccessContext[Task, _, _],
      response: ServerResponse[_]
  ): Task[Unit] =
    logger.log(
      s"Request ${ctx.request} handled by: ${ctx.endpoint.show}; responding with code: ${response.code.code})"
    )

  override def exception(
      e: AnyEndpoint,
      request: ServerRequest,
      ex: Throwable
  ): Task[Unit] =
    logger.log(s"Exception $ex when handling request $request by: ${e.show}")
}

object HttpRequestLogger {
  val layer: URLayer[Has[Logger[String]], Has[ServerLog[Task]]] = (HttpRequestLogger.apply _).toLayer
}
