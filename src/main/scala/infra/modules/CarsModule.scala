package infra.modules

import domain.Cars.{Car, CarRegistrationPlate, CarsStats}
import infra.db.CarsError
import services.CarsService
import zio._
import sttp.tapir.ztapir._
import sttp.tapir.generic.auto._
import sttp.tapir.json.circe._
import sttp.tapir.codec.refined._
import io.circe._
import io.circe.refined._
import io.circe.generic.auto._


case class CarsModule(service: CarsService) extends Module {
  val swaggerTag: String = "Cars"

  private val listCars: AppServerEndpoint = endpoint.get
    .in("car")
    .in(query[Option[CarRegistrationPlate]]("registrationPlate"))
    .in(query[Option[String]]("manufacturer"))
    .in(carsOrderInput)
    .out(jsonBody[List[Car]])
    .errorOut(plainBody[Throwable])
    .serverLogicWithBusinessError[CarsError]((service.listCars _).tupled)

  private val addCar: AppServerEndpoint = endpoint.put
    .in("car")
    .in(jsonBody[Car])
    .errorOut(plainBody[Throwable])
    .serverLogicWithBusinessError[CarsError](service.addCar)

  private val removeCar: AppServerEndpoint = endpoint.delete
    .in("car")
    .in(query[CarRegistrationPlate]("registrationPlate"))
    .errorOut(plainBody[Throwable])
    .serverLogicWithBusinessError[CarsError](service.removeCar)

  private val carStats: AppServerEndpoint = endpoint.get
    .in("car" / "stats")
    .errorOut(plainBody[Throwable])
    .out(jsonBody[CarsStats])
    .serverLogicWithBusinessError[CarsError](_ => service.carsStats())

  val httpEndpoints: List[AppServerEndpoint] =
    listCars :: addCar :: removeCar :: carStats :: Nil
}

object CarsModule {
  val layer = (CarsModule.apply _).toLayer
}
