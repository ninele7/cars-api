package infra.modules

import sttp.tapir.server.ServerEndpoint
import sttp.tapir.ztapir._
import zio._

case class HelloWorldModule() extends Module {
  val swaggerTag = "Hello world"

  private def countCharacters(s: String): ZIO[Any, Unit, Int] =
    ZIO.succeed(s.length)

  private val countCharactersEndpoint: AppServerEndpoint = endpoint
    .post
    .in("countCharacters")
    .in(stringBody)
    .out(plainBody[Int])
    .zServerLogic(countCharacters)

  val httpEndpoints: List[ServerEndpoint[Any, Task]] = countCharactersEndpoint :: Nil
}

object HelloWorldModule {
  val layer = (HelloWorldModule.apply _).toLayer
}
