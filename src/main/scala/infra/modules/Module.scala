package infra.modules


trait Module {
  def swaggerTag: String

  def httpEndpoints: List[AppServerEndpoint]

  def taggedEndpoints: List[AppServerEndpoint] =
    httpEndpoints.map(_.tag(swaggerTag))
}
