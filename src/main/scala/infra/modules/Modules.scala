package infra.modules

import infra.http.server.AppEndpoint
import io.prometheus.client.CollectorRegistry
import sttp.tapir.metrics.prometheus.PrometheusMetrics
import sttp.tapir.server.interceptor.log.ServerLog
import sttp.tapir.server.ziohttp.{ZioHttpInterpreter, ZioHttpServerOptions}
import sttp.tapir.swagger.bundle.SwaggerInterpreter
import zio._

case class Modules(
    helloWorldModule: HelloWorldModule,
    carsModule: CarsModule,
    httpRequestLogger: ServerLog[Task]
) {
  import Modules._

  private val zioHttpServerOptions: ZioHttpServerOptions[Any] =
    ZioHttpServerOptions.customInterceptors
      .serverLog(httpRequestLogger)
      .metricsInterceptor(
        prometheusMetrics.metricsInterceptor()
      )
      .options

  val httpEndpoint: AppEndpoint = {
    val endpoints = this.productIterator.flatMap {
      case m: Module => Some(m)
      case _         => None
    }.flatMap(_.taggedEndpoints).toList
    val swagger = SwaggerInterpreter(pathPrefix = "swagger" :: Nil)
      .fromEndpoints[Task](
        endpoints.map(_.endpoint),
        "Cars API",
        "V1"
      )
    ZioHttpInterpreter(zioHttpServerOptions).toHttp(
      endpoints ::: swagger ::: prometheusMetrics.metricsEndpoint :: Nil
    )
  }
}

object Modules {
  val layer = (Modules.apply _).toLayer

  private val prometheusMetrics =
    PrometheusMetrics[Task](
      "tapir",
      CollectorRegistry.defaultRegistry
    ).withRequestsTotal().withResponsesDuration()
}
