package infra

import eu.timepit.refined.api.Refined
import infra.db.{CarsError, OrderDirection}
import io.circe.{Decoder, Encoder}
import sttp.tapir.EndpointIO.annotations.query
import sttp.tapir.{Codec, CodecFormat, Endpoint, EndpointInput, Schema, Validator}
import sttp.tapir.ztapir.ZServerEndpoint
import sttp.tapir.codec.enumeratum._
import sttp.tapir.server.ServerEndpoint
import zio.{Task, ZIO}

import scala.language.implicitConversions
import scala.reflect.ClassTag

package object modules {
  type AppServerEndpoint = ZServerEndpoint[Any, Any]

  implicit val ThrowableCodec: Codec[String, Throwable, CodecFormat.TextPlain] =
    Codec.string.map(_ => new Exception: Throwable) {
      case e: CarsError => e.getMessage
      case _            => "Internal server error"
    }

  implicit def enumDecoder(e: Enumeration): Decoder[e.Value] =
    Decoder.decodeEnumeration(e)
  implicit def enumEncoder(e: Enumeration): Encoder[e.Value] =
    Encoder.encodeEnumeration(e)

  implicit def schemaForEnum(e: Enumeration): Schema[e.Value] =
    Schema.string.validate(
      Validator.enumeration(e.values.toList, v => Option(v))
    )

  case class CarsOrderInput(
      @query("orderField")
      field: Option[String],
      @query("orderDirection")
      direction: Option[OrderDirection]
  )

  val carsOrderInput: EndpointInput[CarsOrderInput] =
    EndpointInput.derived[CarsOrderInput]

  implicit class EndpointLogicExtension[A, I, O, C](
      e: Endpoint[A, I, Throwable, O, C]
  ) {
    def serverLogicWithBusinessError[E <: Throwable: ClassTag](
        logic: I => Task[O]
    )(implicit aIsUnit: A =:= Unit): ZServerEndpoint[Any, C] =
      e.serverLogic(
        logic.andThen(e =>
          e.map(Right(_)).catchSome { case c: E =>
            Task.left(c: Throwable)
          }
        )
      )
  }
}
