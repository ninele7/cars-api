package repositories

import domain.Cars.{Car, CarRegistrationPlate, CarsStats}
import doobie.implicits._
import doobie.util.transactor.Transactor
import doobie.postgres._
import doobie.postgres.implicits._
import doobie.refined.implicits._
import zio._
import zio.interop.catz._
import doobie.{Fragment, Fragments}
import infra.db.{CarAlreadyExists, CarNotPresent, OrderDirection}
import infra.modules.CarsOrderInput

trait CarsRepository {
  def listCars(
      registrationPlate: Option[CarRegistrationPlate],
      manufacturer: Option[String],
      order: CarsOrderInput
  ): Task[List[Car]]

  def addCar(car: Car): Task[Unit]

  def removeCar(registrationPlate: CarRegistrationPlate): Task[Unit]

  def carsStats(): Task[CarsStats]
}

case class CarsRepositoryLive(xa: Transactor[Task]) extends CarsRepository {
  override def listCars(
      registrationPlate: Option[CarRegistrationPlate],
      manufacturer: Option[String],
      order: CarsOrderInput
  ): Task[List[Car]] = {
    val filters = Fragments.whereAndOpt(
      registrationPlate.map(n => fr"registration_plate = $n"),
      manufacturer.map(n => fr"manufacturer = $n")
    )
    val orderFr = order.field.fold(fr"")(f =>
      fr"order by ${Fragment.const(f)}" ++
        (order.direction.getOrElse(OrderDirection.Ascending) match {
          case OrderDirection.Ascending  => fr"asc"
          case OrderDirection.Descending => fr"desc"
        })
    )
    (fr"select registration_plate, manufacturer, color, production_year from cars" ++ filters ++ orderFr)
      .query[Car]
      .to[List]
      .transact(xa)
  }

  override def addCar(car: Car): Task[Unit] =
    sql"""insert into cars (registration_plate, manufacturer, color, production_year)
          values (${car.registrationPlate}, ${car.manufacturer}, ${car.color}, ${car.productionYear})""".update.run
      .attemptSomeSqlState { case sqlstate.class23.UNIQUE_VIOLATION =>
        CarAlreadyExists
      }
      .transact(xa)
      .flatMap(Task.fromEither(_))
      .unit

  override def removeCar(registrationPlate: CarRegistrationPlate): Task[Unit] =
    sql"delete from cars where registration_plate = $registrationPlate".update.run
      .transact(xa)
      .flatMap {
        case 0 => Task.fail(CarNotPresent)
        case _ => Task.unit
      }

  override def carsStats(): Task[CarsStats] =
    sql"select count(*), min(created_at), max(created_at) from cars"
      .query[CarsStats]
      .unique
      .transact(xa)
}

object CarsRepository {
  val layer: URLayer[Has[Transactor[Task]], Has[CarsRepository]] =
    (CarsRepositoryLive.apply _).toLayer
}
