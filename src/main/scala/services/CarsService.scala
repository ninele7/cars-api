package services

import domain.Cars.{Car, CarRegistrationPlate, CarsStats}
import infra.db.IncorrectOrderingField
import infra.modules.CarsOrderInput
import repositories.CarsRepository
import zio._
import zio.logging.Logger

trait CarsService {
  def listCars(
      registrationPlate: Option[CarRegistrationPlate],
      manufacturer: Option[String],
      order: CarsOrderInput
  ): Task[List[Car]]

  def addCar(car: Car): Task[Unit]

  def removeCar(registrationPlate: CarRegistrationPlate): Task[Unit]

  def carsStats(): Task[CarsStats]
}

case class CarsServiceLive(repository: CarsRepository, logger: Logger[String])
    extends CarsService {
  import CarsService._

  override def listCars(
      registrationPlate: Option[CarRegistrationPlate],
      manufacturer: Option[String],
      order: CarsOrderInput
  ): Task[List[Car]] = for {
    _ <- Task.cond(
      order.field.forall(f => carOrderFieldNames.contains(f)),
      (),
      IncorrectOrderingField
    )
    result <- repository.listCars(registrationPlate, manufacturer, order)
  } yield result

  override def addCar(car: Car): Task[Unit] = repository.addCar(car)

  override def removeCar(registrationPlate: CarRegistrationPlate): Task[Unit] =
    repository.removeCar(registrationPlate)

  override def carsStats(): Task[CarsStats] = repository.carsStats()
}

object CarsService {
  val layer: URLayer[Has[CarsRepository] with Has[Logger[String]], Has[
    CarsService
  ]] =
    (CarsServiceLive.apply _).toLayer

  val carOrderFieldNames: Seq[String] =
    "registration_plate" :: "manufacturer" :: "color" :: "production_year" :: Nil
}
